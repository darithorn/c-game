#ifndef __ILIAD_ENTITY__
#define __ILIAD_ENTITY__

#include <orx/orx.h>

#define ILIAD_INTERACT_MAX_DIST 120

orxLINKLIST entities;

typedef enum {
    ANIM_PLAY_LOOP,
    ANIM_PLAY_ONCE
} iliadANIM_PLAY_TYPE;

typedef struct {
    orxLINKLIST_NODE node;
    orxANIM *anim;
    iliadANIM_PLAY_TYPE playType;
} iliadANIM;

iliadANIM *iliadAnim_Create(const orxSTRING name) {
    iliadANIM *tmp = orxMemory_Allocate(sizeof(iliadANIM), orxMEMORY_TYPE_MAIN);
    tmp->anim = orxAnim_CreateFromConfig(name);
    orxConfig_SelectSection(name);
    const orxSTRING playType = orxConfig_GetString("PlayType");
    if(playType != orxSTRING_EMPTY) {
        if(!orxString_Compare(playType, "Loop")) {
            tmp->playType = ANIM_PLAY_LOOP;
        } else if(!orxString_Compare(playType, "Once")) {
            tmp->playType = ANIM_PLAY_ONCE;
        }
    }
    return tmp;
}

typedef struct {
    orxANIMSET *set;
    orxLINKLIST anims;
} iliadANIMSET;

iliadANIMSET *iliadAnimSet_Create(orxU32 count) {
    iliadANIMSET *tmp = orxMemory_Allocate(sizeof(iliadANIMSET), orxMEMORY_TYPE_MAIN);
    tmp->set = orxAnimSet_Create(count);
    return tmp;
}

void iliadAnimSet_AddAnim(iliadANIMSET *animSet, const iliadANIM *anim) {
    orxAnimSet_AddAnim(animSet->set, anim->anim);
    orxLinkList_AddEnd(&animSet->anims, (orxLINKLIST_NODE *)anim);
}

typedef struct iliadENTITY iliadENTITY;
typedef void (*iliadINTERACT)(iliadENTITY *self);

struct iliadENTITY {
    orxLINKLIST_NODE node;
    orxOBJECT *obj;
    orxU32 id;
    orxBOOL interactable;
    orxBOOL hovered;
    orxBOOL interacted; // whether the entity's interact state is on or off
    iliadINTERACT interact;
    const orxSTRING *offTexts;
    orxS32 offTextsLength;
    const orxSTRING *onTexts;
    orxS32 onTextsLength;
    orxOBJECT *text;
    orxANIMSET *animSet;
};

void iliadEntity_Interact(iliadENTITY *self) {
    orxLOG("%s has no interact function", orxObject_GetName(self->obj));
}

iliadENTITY *iliadEntity_Create(const orxSTRING name) {
    iliadENTITY *tmp = orxMemory_Allocate(sizeof(iliadENTITY), orxMEMORY_TYPE_MAIN);
    if(tmp == orxNULL) {
        orxLOG("could not create new iliadENTITY");
        return tmp;
    }
    tmp->obj = orxObject_CreateFromConfig(name);
    orxConfig_SelectSection(name);
    orxS32 animCount = orxConfig_GetListCounter("Animations");
    tmp->animSet = orxAnimSet_Create(animCount);
    int i = 0;
    for(; i < animCount; i++) {
        orxANIM *anim = orxAnim_CreateFromConfig(orxConfig_GetListString("Animations", i));
        orxAnimSet_AddAnim(tmp->animSet, anim);
    }
    orxObject_SetAnimSet(tmp->obj, tmp->animSet);
    tmp->id = orxConfig_GetU32("ID");
    tmp->interactable = orxConfig_GetBool("Interactable");
    tmp->hovered = orxFALSE;
    tmp->interacted = orxFALSE;
    tmp->interact = &iliadEntity_Interact;
    tmp->offTextsLength = orxConfig_GetListCounter("InteractOffText");
    tmp->offTexts = orxMemory_Allocate(sizeof(orxSTRING) * tmp->offTextsLength, orxMEMORY_TYPE_MAIN);
    i = 0;
    for(; i < tmp->offTextsLength; i++) {
        tmp->offTexts[i] = orxConfig_GetListString("InteractOffText", i);
    }
    tmp->onTextsLength = orxConfig_GetListCounter("InteractOnText");
    tmp->onTexts = orxMemory_Allocate(sizeof(orxSTRING) * tmp->onTextsLength, orxMEMORY_TYPE_MAIN);
    i = 0;
    for(; i < tmp->onTextsLength; i++) {
        tmp->onTexts[i] = orxConfig_GetListString("InteractOnText", i);
    }
    tmp->text = orxNULL;
    orxLinkList_AddEnd(&entities, (orxLINKLIST_NODE *)tmp);
    return tmp;
}

iliadENTITY *iliadEntity_CreateInteractable(const orxSTRING name, iliadINTERACT interact) {
    iliadENTITY *tmp = iliadEntity_Create(name);
    tmp->interact = interact;
    return tmp;
}

void iliadEntity_Delete(iliadENTITY* entity) {
    if(entity->text != orxNULL) orxObject_Delete(entity->text);
    if(entity->offTextsLength != 0) orxMemory_Free(entity->offTexts);
    orxAnimSet_Delete(entity->animSet);
    orxMemory_Free(entity);
}

void iliadEntity_Hover(iliadENTITY* entity, iliadENTITY *player) {
    if(!entity->interactable) return;
    if(entity->offTextsLength == 0) { 
        orxLOG("%s has no interact offTexts", orxObject_GetName(entity->obj));
        return;
    }
    orxVECTOR player_pos, entity_pos;
    orxObject_GetPosition(player->obj, &player_pos);
    orxObject_GetPosition(entity->obj, &entity_pos);
    if(orxVector_GetDistance(&player_pos, &entity_pos) > ILIAD_INTERACT_MAX_DIST) return;
    entity->hovered = orxTRUE;
    if(entity->text == orxNULL) {
        entity->text = orxObject_CreateFromConfig("oInteractText");
        orxVECTOR pos, size;
        orxObject_GetPosition(entity->obj, &pos);
        orxObject_GetSize(entity->obj, &size);
        pos.fY -= ((size.fY * 8) / 2.0f) + (8 * 2);
        orxObject_SetPosition(entity->text, &pos);
    }
    if(entity->interacted) {
        orxU32 index = orxMath_GetRandomU32(0, entity->onTextsLength - 1);
        if(!orxObject_SetTextString(entity->text, entity->onTexts[index])) {
            orxLOG("problem setting text string");
        }
    } else {
        orxU32 index = orxMath_GetRandomU32(0, entity->offTextsLength - 1);
        if(!orxObject_SetTextString(entity->text, entity->offTexts[index])) {
            orxLOG("problem setting text string");
        }
    }
}

void iliadEntity_HoverExit(iliadENTITY *entity) {
    if(!entity->interactable || !entity->hovered) return;
    entity->hovered = orxFALSE;
    orxObject_SetTextString(entity->text, "");
}

iliadENTITY *iliadEntity_GetByID(orxU32 id) {
    orxLINKLIST_NODE *cur;
    for(cur = orxLinkList_GetFirst(&entities); cur != orxNULL; cur = orxLinkList_GetNext(cur)) {
        iliadENTITY *entity = (iliadENTITY *)cur;
        if(entity->id == id) return entity;
    }
    orxLOG("could not find entity with id %u", id);
    return orxNULL;
}

#endif