#ifndef __HOUSE_TV__
#define __HOUSE_TV__

#include "iliad_entity.c"

#define A_HOUSE_TV_OFF "aHouseTVOff"
#define A_HOUSE_TV_ON "aHouseTVOn"

void iliadHouseTV_Interact(iliadENTITY *self) {
	if(self->interacted) {
        orxObject_SetCurrentAnim(self->obj, A_HOUSE_TV_OFF);
        self->interacted = orxFALSE;
    } else {
        orxObject_SetCurrentAnim(self->obj, A_HOUSE_TV_ON);
        self->interacted = orxTRUE;
    }
}

#define A_HOUSE_CONSOLE_OFF "aHouseConsoleOff"
#define A_HOUSE_CONSOLE_ON "aHouseConsoleOn"

void iliadHouseConsole_Interact(iliadENTITY *self) {
	if(self->interacted) {
        orxObject_SetCurrentAnim(self->obj, A_HOUSE_CONSOLE_OFF);
        self->interacted = orxFALSE;
    } else {
        orxObject_SetCurrentAnim(self->obj, A_HOUSE_CONSOLE_ON);
        self->interacted = orxTRUE;
    }
}

#endif