#include "pepper.h"

#define SOURCE_FILES "main.c"

int main() {
#if defined(__linux__)
	pepper_set_compiler("gcc");
	pepper_add_compiler_flag(SOURCE_FILES);
	pepper_add_compiler_flag("-o main");
	pepper_add_compiler_flag("-I ./src/ -I ~/.usr/include/ -I ~/.usr/include/orx/");
	pepper_add_compiler_flag("-L ~/.usr/lib");
	pepper_add_compiler_flag("-lorxd -lopenal -lm");
#endif
	pepper_build(true);
	pepper_output_errors();
	return 0;
}