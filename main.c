#include <orx/orx.h>
#include "iliad_entity.c"
#include "house_tv.c"

#define ILIAD_INTERACTABLE_ID 0x0001

typedef struct {
    iliadENTITY *entity;
    orxFLOAT maxSpeed;
    orxFLOAT acceleration;
    orxFLOAT dampening;
    orxFLOAT currentSpeed;
} iliadPLAYER;

iliadPLAYER *iliadPlayer_Create() {
    iliadPLAYER *tmp = orxMemory_Allocate(sizeof(iliadPLAYER), orxMEMORY_TYPE_MAIN);
    if(tmp == orxNULL) {
        orxLOG("could not create iliadPLAYER");
        return tmp;
    }
    tmp->entity = iliadEntity_Create("Player");
    orxConfig_SelectSection("Player");
    tmp->maxSpeed = orxConfig_GetFloat("MaxSpeed");
    tmp->acceleration = orxConfig_GetFloat("Acceleration");
    tmp->dampening = orxConfig_GetFloat("Dampening");
    tmp->currentSpeed = 0.0f;
    return tmp;
}

orxVIEWPORT *viewport;
orxCAMERA *camera;
iliadPLAYER *player;
iliadENTITY *house;

void iliadLogVector(orxVECTOR *vec) {
    orxLOG("x: %f\ty: %f\tz: %f", vec->fX, vec->fY, vec->fZ);
}

void iliadLogOBox(orxOBOX *box) {
    orxLOG("\nvPos { x: %f, y: %f, z: %f }\n\
vPiv: { x: %f, y: %f, z: %f }\n\
vX: { x: %f, y: %f, z: %f }\n\
vY: { x: %f, y: %f, z: %f }\n\
vZ: { x: %f, y: %f, z: %f }", 
        box->vPosition.fX, box->vPosition.fY, box->vPosition.fZ,
        box->vPivot.fX, box->vPivot.fY, box->vPivot.fZ, 
        box->vX.fX, box->vX.fY, box->vX.fZ,
        box->vY.fX, box->vY.fY, box->vY.fZ,
        box->vZ.fX, box->vZ.fY, box->vZ.fZ);
}

void orxFASTCALL update(const orxCLOCK_INFO *clockInfo, void *context) {
    orxVECTOR pos;
    orxObject_GetPosition(player->entity->obj, &pos);
    if(orxInput_IsActive("Left")) {
        player->currentSpeed -= player->acceleration * clockInfo->fDT;
    } else if(orxInput_IsActive("Right")) {
        player->currentSpeed += player->acceleration * clockInfo->fDT;
    }
    player->currentSpeed *= player->dampening;
    if(player->currentSpeed >= player->maxSpeed) {
        player->currentSpeed = player->maxSpeed;
    } else if(player->currentSpeed <= -player->maxSpeed) {
        player->currentSpeed = -player->maxSpeed;
    }
    pos.fX += player->currentSpeed;
    orxObject_SetPosition(player->entity->obj, &pos);

    orxVECTOR wPos;
    orxMouse_GetPosition(&pos);
    orxRender_GetWorldPosition(&pos, viewport, &wPos);
    wPos.fZ = 0;
    orxLINKLIST_NODE *cur;
    for(cur = orxLinkList_GetFirst(&entities); cur != orxNULL; cur = orxLinkList_GetNext(cur)) {
        iliadENTITY *entity = (iliadENTITY *)cur;
        if(entity->interactable) {
            orxOBOX box;
            orxObject_GetBoundingBox(entity->obj, &box);
            if(orxOBox_IsInside(&box, &wPos) == orxTRUE) {
                // Do something with the interactable
                iliadEntity_Hover(entity, player->entity);
                if(orxInput_IsActive("LeftMouse") && orxInput_HasNewStatus("LeftMouse")) {
                    if(entity->interact != orxNULL) entity->interact(entity);
                }
                break;
            } else {
                iliadEntity_HoverExit(entity);
            }
        }
    }
}

orxSTATUS orxFASTCALL init() {
    orxCLOCK *updateClock;
    updateClock = orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE);
    orxClock_Register(updateClock, update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

    viewport = orxViewport_CreateFromConfig("Viewport");
    camera = orxViewport_GetCamera(viewport);

    player = iliadPlayer_Create();
    house = iliadEntity_Create("oHouse");

    iliadEntity_CreateInteractable("oHouseTV", &iliadHouseTV_Interact);
    iliadEntity_CreateInteractable("oHouseConsole", &iliadHouseConsole_Interact);

    return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL run() {
    orxVECTOR pos;
    orxObject_GetPosition(player->entity->obj, &pos);
    pos.fZ = -1.0f;
    orxCamera_SetPosition(camera, &pos);
    return orxSTATUS_SUCCESS;
}

void orxFASTCALL quit() {
    iliadEntity_Delete(player->entity);
    iliadEntity_Delete(house);
}

int main(int argc, char **argv) {
    orx_Execute(argc, argv, init, run, quit);
    return 0;
}